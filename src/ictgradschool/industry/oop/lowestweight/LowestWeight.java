package ictgradschool.industry.oop.lowestweight;

import ictgradschool.Keyboard;

/**
 * Write a program which asks the user to enter the weights of two people,
 * then uses Math.min() to determine the lowest weight, and prints out the result.
 */

public class LowestWeight {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    private void start() {

        System.out.print("Enter first person's weight: ");
        double weight1 = Double.parseDouble(Keyboard.readInput());
        System.out.println(weight1);
        System.out.print("Enter second person's weight: ");
        double weight2 = Double.parseDouble(Keyboard.readInput());
        System.out.println(weight2);
        System.out.println("Lowest weight is " + Math.min(weight1, weight2));
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        LowestWeight ex = new LowestWeight();
        ex.start();

    }
}
