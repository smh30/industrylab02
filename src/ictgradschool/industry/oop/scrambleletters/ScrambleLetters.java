package ictgradschool.industry.oop.scrambleletters;

import ictgradschool.Keyboard;
import ictgradschool.industry.oop.noughtsandcrosses.NoughtsAndCrosses;

public class ScrambleLetters {

    private void start() {

        String word = getWord();

        String newWord = rearrangeWord(word);

        String letter = userLetter();

        printPosition(word, letter);

        printScramble (newWord);
    }

    public String getWord(){
        System.out.print("Enter a word: ");
        String word = Keyboard.readInput();
        return word;
    }

    private String rearrangeWord(String word){
        String lettersRemaining = word;
        String newWord = "";
        for (int i = 0; i< word.length(); i++){
            int randomPosition = (int)(Math.random() * lettersRemaining.length());
            newWord += lettersRemaining.charAt(randomPosition);
            lettersRemaining = lettersRemaining.substring(0, randomPosition)
 + lettersRemaining.substring(randomPosition + 1);
        }
        return newWord;
    }

    private String userLetter(){
        System.out.print("Choose a letter: ");
        String ch = Keyboard.readInput();
        return ch;
    }

    private void printPosition(String word, String ch){
        int pos = word.indexOf(ch);
        System.out.println("This letter is now in position " + pos);
    }

    private void printScramble(String newWord) {
        System.out.print("The scrambled word is " + newWord);
    }

    public static void main(String[] args) {
        ScrambleLetters ex = new ScrambleLetters();
        ex.start();
    }
}
