package ictgradschool.industry.oop.sortingnumbers;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class SortingNumbers {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */


   private int getRandom(int inputA, int inputB) {
       int diff = (Math.max(inputA, inputB) - (Math.min(inputA, inputB))) + 1;

       int output = (int)(Math.random() * (diff) + Math.min(inputA, inputB));
       return output;
   }

    private void start() {
        System.out.print("Lower bound? ");
        int inputA = Integer.parseInt(Keyboard.readInput());
//        System.out.println(inputA);
        System.out.print("Upper bound? ");
        int inputB = Integer.parseInt(Keyboard.readInput());
//        System.out.println(inputB);
        System.out.print("3 randomly generated numbers: ");
        int r1 = getRandom( inputA, inputB);
        int r2 = getRandom( inputA, inputB);
        int r3 = getRandom( inputA, inputB);
        System.out.println(r1 + ", " + r2 + " and " + r3);
        System.out.print("Smallest number is " + Math.min(r1, Math.min(r2, r3)));






    }
    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        SortingNumbers ex = new SortingNumbers();
        ex.start();

    }
}
