package ictgradschool.industry.oop.removechar;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class RemoveCharacter {

    private void start() {

        String sentence = getSentenceFromUser();

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return
     */
    private String getSentenceFromUser() {
        System.out.print("Enter a sentence: ");
        String sentence = Keyboard.readInput();

        // TODO Prompt the user to enter a sentence, then get their input.
        return sentence;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {
        int position = (int)(sentence.length() * Math.random());
        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.
        return position;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {
        System.out.println("Removing \"" + sentence.charAt(position) + "\" from position " + (position + 1));


        // TODO Implement this method

    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence, int position) {
        String s1 = sentence.substring(0, position);
        String s2 = sentence.substring(position + 1);
        String changedSentence = s1 + s2;

        // TODO Implement this method
        return changedSentence;

    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {
        System.out.print("New sentence is: " + changedSentence);
        // TODO Implement this method
    }

    public static void main(String[] args) {
        RemoveCharacter ex = new RemoveCharacter();
        ex.start();
    }
}
