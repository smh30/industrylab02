package ictgradschool.industry.oop.truncateamount;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:
 *
 * 1. Printing the prompt and reading the amount from the user
 * 2. Printing the prompt and reading the number of decimal places from the user
 * 3. Truncating the amount to the user-specified number of DP's
 * 4. Printing the truncated amount
 *
 */
public class TruncateAmount {

    private void start() {
        String amount = userAmount();
        int decimal = decimalPlaces();
        String cut = truncatedAmount(amount, decimal);
        printCut(cut, decimal);

        // TODO Use other methods you create to implement this program's functionality.
    }

    private String userAmount(){
        System.out.print("Please enter an amount: ");
        String amount = Keyboard.readInput();
        return amount;
    }

    private int decimalPlaces(){
        System.out.print("Please enter the number of decimal places: ");
        int dp = Integer.parseInt(Keyboard.readInput());
        return dp;
    }

    private String truncatedAmount(String amount, int dp){
        int pos = amount.indexOf('.');
        String cut = amount.substring(0, pos + dp +1);
        return cut;
    }

    private void printCut(String cut, int decimal){
        System.out.print("Amount truncated to " + decimal + " decimal places is: " + cut);
    }



    // TODO Write a method which prints the truncated amount

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        TruncateAmount ex = new TruncateAmount();
        ex.start();
    }
}
